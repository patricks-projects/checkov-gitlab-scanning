import sys
import json
import hashlib


def load_json_file(file_name):
    """
    Attempts to load the json file from the disk and returns a json object containing the data.
    Returns an empty reference if no data exists in the file.
    :param file_name:
    :return:
    """
    with open(file_name, "r") as file:
        json_file = file.read().replace('\n', '')

    if json_file == '':
        print('no data found within the file, exiting')
        return ""

        # Parse into json Object
    return json.loads(json_file)


def check_json_for_results(json_object):
    """
    Test if there are any failed checks within the checkov json object. Return true if there is at least one
    failed check.

    :param json_object:
    :return:
    """
    if "results" in json_object\
            and "failed_checks" in json_object["results"]\
            and len(json_object["results"]["failed_checks"]) > 0:

        print("There is at least one failed check, return true")
        return True
    else:
        print("No failed checks, returning false")
        return False


def build_gitlab_vulnerability_from_checkov_vulnerability(vuln):
    # Guidance is not required, so check if it's present before we build our vuln
    message = ''
    if 'guideline' in vuln:
        message = vuln['guideline']

    # build multi-line for the code block for hashing
    code_block = ''
    for block in vuln['code_block']:
        code_block = code_block + block[1] + "\n"

    # The same code block can have multiple findings, so add the check ID to it as well.
    check_string = vuln['check_id']
    md5hash = hashlib.md5((check_string + code_block).encode('utf-8')).hexdigest()

    new_vulnerability = {
        "id": md5hash,
        "category": "sast",
        "name": vuln['check_name'],
        "message": vuln['check_id'] + " : " + vuln['check_name'],
        "description": vuln['check_id'] + " : " + vuln['check_name'],
        "cve": "",  # required, but deprecated
        "severity": "Medium",
        "confidence": "High",
        "location": {
            "file": vuln['file_path'],
            "start_line": vuln['file_line_range'][0],
            "end_line": vuln['file_line_range'][1],
            "class": vuln['resource']
        },
        "scanner": {
            "id": "chekov-scanner",
            "name": "Checkov Scanner for Terraform"
        },
        "links": [{
            "url": message
        }],
        "identifiers": [
            {
                "type": "checkov",
                "name": vuln['check_name'],
                "value": vuln['check_id'],
                "url": message
            }
        ]
    }

    return new_vulnerability


def build_gitlab_report(json_object, output_dictionary):
    """
    Loops through the checkov report and builds out a gitlab JSON for the checkov report.
    :param json_object:
    :param output_dictionary:
    :return:
    """
    for vulnerability in json_object["results"]["failed_checks"]:
        new_vulnerability = build_gitlab_vulnerability_from_checkov_vulnerability(vulnerability)
        output_dictionary["vulnerabilities"].append(new_vulnerability)

    return output_dictionary


def main(file_name):
    """
    Coordinate building out the vuln file.
    :return:
    """
    # Set up the base requirements for the scan.
    output_dictionary = {
        "version": "3.0.1",  # This is the version of the scan schema on gitlab, not our scanner.
        "vulnerabilities": []
    }

    json_object = load_json_file(file_name)

    # If only one scan type is run, then the object returned is just a normal JSON object. If two scans or more
    # are run, it's an array. If the object is not an array, convert it to one so we can treat all downstream
    # objects as an array.
    if not isinstance(json_object, list):
        json_object = [json_object]

    for obj in json_object:
        if check_json_for_results(obj):
            build_gitlab_report(obj, output_dictionary)
        else:
            print("printing an empty vuln report")

    return json.dumps(output_dictionary)


if __name__ == "__main__":
    print(main(sys.argv[1]))

