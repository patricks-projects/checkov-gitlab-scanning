import setuptools
import os

module_version = os.environ["MODULE_VERSION"]

setuptools.setup(
    name="checkov-gitlab-scanner",
    version=module_version,
    author="Patrick Rice",
    author_email="rice.patrick@gmail.com",
    description="Package to translate the checkov json format to the AST format for gitlab",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    url="https://gitlab.com/rice-patrick/checkov-gitlab-scanning",
    python_requires='>=3.6',
)